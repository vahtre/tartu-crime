# Tartu crime

IDS2019 Project

Our goal was to find correlations between weather and crime in Tartu years 2014-2019 and also see how did Metallica concert affect the crime rate on 15-20 July 2019.

The code works without errors if runned from the top to bottom.

Our code reads in the seven datasets, that we used:

* Dataset about breach of the peace or crimes committed in public spaces in Estonia in 2018-2019. 
* Dataset about breach of the peace or crimes committed in public spaces in Estonia in 2014-2018.
* Dataset about property crimes committed in Estonia in 2018-2019. 
* Dataset about property crimes committed in Estonia in 2014-2018.
* Dataset about traffic crimes committed in Estonia in 2018-2019. 
* Dataset about traffic crimes committed in Estonia in 2014-2018.
* Dataset about recorded weather conditions in Tartu.

All the crime datasets are available here https://www.politsei.ee/et/avaandmed and the weather data here http://meteo.physic.ut.ee/.

As we created the code in the work process it might be a bit hard to follow, but it first selects only rows about Tartu from the crime datasets. Then studies the datasets' columns rows and values, discards some columns like the paragraphs of crime, that are no interest to us
and joins the datasets together. We studied all the sets together, only traffic crimes 2014-2019, public and property crimes together 2014-2019, all crime of 2019, all crime of 2014-2018 separately to see the estimates and also
together with weather data of 2014-2019.

In our final dataframes we had columns: date, event type, crime type, event type count, crime type count, crime type sum, crime type mean, temperature max, temperature min, temperature mean, wind speed max,
wind speed min, wind speed mean, rainfall max, rainfall min, rainfall mean, fallen snow max, fallen snow min, fallen snow mean.
All were the calculated estimates per date.

Our code then finds the correlations in the tables and shows them in heatmaps. Then it tries to train some K-nn and Decision tree models, which is unsuccessful because the correlations are too low.

Then the code creates some scatterplots of crime and temperature, crime and rainfall, crime and wind, crime and snow for years 2014-2019 and 2019 alone.

Then the code shows plots of crime and temperature means in the course of years. Then barplots, that study the balance between criminal offences and misdemeanors in different crime types.

Finally we extract the Metallica dates 15-20 July 2019 and study these separately. The estimates and how do they compare to the averages of 2014-2019, 2014-2018 and 2019.
And show the findings on bar plots.

Almost all the plots are saved by the code as 300 dpi images, that we used for the poster.