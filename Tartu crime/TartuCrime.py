#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# allows to output plots in the notebook
get_ipython().run_line_magic('matplotlib', 'inline')

# Set the default style
plt.style.use("ggplot")


# In[2]:


crime1 = pd.read_csv("avalik_1.csv", sep="\t") #header=None estonia public crime 2018-2019
crime2 = pd.read_csv("avalik_2.csv", sep="\t") # public crime 2014-2018 
ilm = pd.read_csv("archive.txt", skipinitialspace=True) # weather 2014-2019


# In[3]:


ilm.describe()


# In[4]:


ilm = ilm.replace(-131.84091, -3.184091)
ilm['Temperatuur'] = ilm.Temperatuur.replace(-131.84091, -3.184091)
ilm.loc[ilm.Temperatuur==-131.84091] = ilm.replace(to_replace=-3.184091)
ilm.describe()


# In[5]:


ilm.head()


# In[6]:


ilm.tail()


# In[7]:


ilm['Sademed'].value_counts()


# In[8]:


ilm['Temperatuur'].mean()


# In[9]:


crime1.columns


# In[10]:


crime2.columns


# In[11]:


ilm.columns


# In[12]:


crime2.head()


# In[13]:


crime1 = crime1.rename(columns={"ToimKpv": "Kuupäev"}) # changing column name 2018-2019
crime1.Kuupäev = pd.to_datetime(crime1.Kuupäev)
tartulinn = crime1[(crime1['ValdLinnNimetus'] == 'Tartu linn') & (crime1['Kuupäev'].dt.year == 2019)] # select only tartu
#tartulinn.truncate(before='2019-01-01')
tartulinn = tartulinn.drop(['Seadus', 'Paragrahv', 'Loige', 'MaakondNimetus', 'Lest_X', 'Lest_Y'], axis=1) # dropping unnecessary columns


# In[14]:


tartulinn.Kuupäev = pd.to_datetime(tartulinn.Kuupäev) # changing data type 2018-2019
# gouping together by date etc
tartu_linn = tartulinn.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']}) 
#tartulinn.truncate(before=pd.Timestamp('2019-01-01'))
tartu_linn.head()


# In[15]:


crime2 = crime2.rename(columns={"ToimKpv": "Kuupäev"}) # changing column name 2014-2018
tartu = crime2[(crime2['ValdLinnNimetus'] == 'Tartu linn')] # select only tartu

#tartu = crime2[(crime2[13] == 'Tartu linn')]
tartu = tartu.drop(['Seadus', 'Paragrahv', 'Loige', 'MaakondNimetus', 'Lest_X', 'Lest_Y'], axis=1) # dropping unnecessary columns


# In[16]:


tartu.Kuupäev = pd.to_datetime(tartu.Kuupäev) # changing data type
#tartu1 = tartu.groupby(['ToimKpv','SyndmusLiik']).agg({'SyndmusLiik': ['count']})
# gouping together by date etc
tartu1 = tartu.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']}) 
tartu1.head()


# In[17]:


tartu.JuhtumId.value_counts()


# In[18]:


tartu.tail()
#tartu[0].value_counts()
#tartu[17].value_counts()
#crimetype_series = tartu[17].value_counts()/len(tartu)
#crimetype_series.plot(kind='bar')


# In[19]:


ilm.Aeg = pd.to_datetime(ilm.Aeg) # change values type
#ilm['Kuupäev'] = ilm.Aeg.date
ilm['Kuupäev'] = ilm['Aeg'].dt.date # making new column for date only
ilm.Kuupäev = pd.to_datetime(ilm.Kuupäev)
#ilm.loc['Kuupäev'] = pd.to_datetime(ilm.Kuupäev) # change value type
#ilm = ilm.loc[(ilm['Kuupäev'] == '2014-01-01': ilm['Kuupäev'] == '2018-12-31', :)] 
#ilm.truncate(after=pd.Timestamp(2018-12-31))

# grouping data together by date
ilm1 = ilm.groupby(['Kuupäev']).agg({'Temperatuur': ['max', 'min', np.mean],'Tuule kiirus': ['max', 'min', np.mean], 'Sademed': ['max', 'min', np.mean], 'Sadanud lumi': ['max', 'min', np.mean]})
#ilm2 = ilm1[(ilm1['Kuupäev'].dt.year != 2019)]
#ilm.truncate(after=509415)
ilm1.tail()
#ilm.loc[(ilm['Aeg'] == '2019-01-01')]


# In[20]:


#ilm2.head()
ilm1.truncate(after=pd.Timestamp(2019-12-31)) # leaving out year 2019
frames = [tartu1, ilm1]
#result = pd.concat([tartu1, ilm1], axis=1)
result = tartu1.join(ilm1) # joining ilm and crime tables together


# In[21]:


kuritegevus = tartu_linn.join(ilm1) # 2019
kuritegevus.head()
kuritegevus.tail()


# In[22]:


#ilm2 = ilm1.loc[(ilm1['Kuupäev'] < '2019-01-01')]
                #& (df['column_name'] <= B)]
#ilm2.tail()
result.tail()


# In[23]:


result.head(205)


# In[24]:


result.columns


# In[25]:


vara = pd.read_csv("vara_2.csv", sep="\t") # estonian crime of property theft 2014-2018
vara.head()


# In[26]:


vara.columns


# In[27]:


vara = vara.rename(columns={"ToimKpv": "Kuupäev"}) # 2014-2019
tartuv = vara[(vara['ValdLinnNimetus'] == 'Tartu linn')] # selecting tartu

tartuv = tartuv.drop(['ToimLoppKpv', 'ToimLoppKell', 'SyndmusTaiendavStatLiik', 'SissetungimiseLiik', 'Valve', 'Seadus', 'Paragrahv', 'ParagrahvTais',
       'Loige', 'Punkt', 'RikutudOigusnorm',
       'MaakondNimetus', 'Lest_X', 'Lest_Y', 'SoidukLiik', 'SoidukMark', 'SoidukVlAasta'], axis=1)
tartuv.Kuupäev = pd.to_datetime(tartuv.Kuupäev)
tartuvara = tartuv.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']})
tartuvara.head()


# In[28]:


result2 = tartuvara.join(ilm1)
result2.head(30)


# In[29]:


tartuavalikvara = pd.concat([tartuv, tartu]) # joining tartu public crimes and property theft 2014-2018
#tartuv.join(tartu, on = ['Kuupäev'], how='inner')
#tartu.merge(tartuv)
#tartu1.merge(tartuvara, on = ['Kuupäev'], how='left')
#tartu1.join(tartuvara, how='outer')
#pd.merge(tartu1, tartuvara, tartu1_index=True, tartuvara_index=True, how='outer')
#tartuvara.join(tartu1, on='Kuupäev')
tartuavalikvara = tartuavalikvara.drop_duplicates()
tartuavalikvara.tail(20)


# In[ ]:





# In[30]:


tartuavalikvara.Kuupäev = pd.to_datetime(tartuavalikvara.Kuupäev) #2014-2018
tartuav = tartuavalikvara.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']})
tartuav.head(25)


# In[31]:


crime3 = pd.read_csv("vara_1.csv", sep="\t", header=None) # estonia property theft 2018-2019
crime3.head()


# In[32]:


crime3[(crime3[18] == 'Tartu maakond') & (crime3[1] == '2019-07-20')]


# In[33]:


crime3 = pd.read_csv("vara_1.csv", sep="\t")
crime3.columns


# In[34]:


crime3 = crime3.rename(columns={"ToimKpv": "Kuupäev"}) #2019
crime3.Kuupäev = pd.to_datetime(crime3.Kuupäev)
tartu2 = crime3[(crime3['ValdLinnNimetus'] == 'Tartu linn') & (crime3['Kuupäev'].dt.year == 2019)]


#tartu = crime2[(crime2[13] == 'Tartu linn')]
tartu2 = tartu2.drop(['ToimLoppKpv', 'ToimLoppKell', 'SyndmusTaiendavStatLiik', 'SissetungimiseLiik', 'Valve', 'Seadus', 'Paragrahv', 'ParagrahvTais',
       'Loige', 'Punkt', 'RikutudOigusnorm',
       'MaakondNimetus', 'Lest_X', 'Lest_Y', 'SoidukLiik', 'SoidukMark', 'SoidukVlAasta'], axis=1)
tartu2.tail()


# In[35]:


tartu2.Kuupäev = pd.to_datetime(tartu2.Kuupäev) # 2019
tartu3 = tartu2.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']})
tartu3.head()
kuritegevus2 = tartu3.join(ilm1)
kuritegevus2.head()
kuritegevus2.tail()


# In[36]:


tartus19 = pd.concat([tartu2, tartulinn]) # joining tartu public crimes and property theft 2019
tartus19 = tartus19.drop_duplicates()
tartus19.tail(10)


# In[37]:


ilmaliikluseta = tartuavalikvara.append(tartus19)
ilmaliikluseta.Kuupäev = pd.to_datetime(ilmaliikluseta.Kuupäev) 
ilmaliikluseta.head()


# In[38]:


ilmaliikluseta = ilmaliikluseta.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']})
ilmaliikluseta.head()


# In[39]:


sum = ilmaliikluseta.groupby(['Kuupäev']).agg({('SyyteoLiik', 'count'): ['sum']})
mean = ilmaliikluseta.groupby(['Kuupäev']).agg({('SyyteoLiik', 'count'): [np.mean]})
resultsum = ilmaliikluseta.join(sum)
ilmaliikluseta = resultsum.join(mean)
ilmaliikluseta.head()


# In[40]:


ilmaliikluseta = ilmaliikluseta.join(ilm1)
ilmaliikluseta.head()


# In[41]:






ilmaliikluseta.reset_index(inplace=True)
ilmaliikluseta.columns = ['Kuupäev', 'SyndmusLiik', 'SyyteoLiik', 'SyndmusLiik count', 'SyyteoLiik count',
       'SyyteoLiik sum', 'SyyteoLiik mean', 'Temperatuur max', 'Temperatuur min', 'Temperatuur mean',
       'Tuule kiirus max', 'Tuule kiirus min', 'Tuule kiirus mean', 'Sademed max', 'Sademed min',
       'Sademed mean', 'Sadanud lumi max', 'Sadanud lumi min', 'Sadanud lumi mean']

ilmaliikluseta.describe()


# In[42]:


ilmaliikluseta.head()


# In[43]:


crime4 = pd.read_csv("liiklus_1.csv", sep="\t") # estonia traffic crime 2018-2019
crime4.columns


# In[44]:


crime4 = crime4.rename(columns={"ToimKpv": "Kuupäev"}) # 2018-2019
crime4.Kuupäev = pd.to_datetime(crime3.Kuupäev) 
tartuliik = crime4[(crime4['ValdLinnNimetus'] == 'Tartu linn') & (crime4['Kuupäev'].dt.year == 2019)]

#tartu = crime2[(crime2[13] == 'Tartu linn')]
tartuliik = tartuliik.drop(['Seadus', 'Paragrahv', 'ParagrahvTais',
       'Loige', 'Punkt', 'RikutudOigusnorm',
       'MaakondNimetus', 'KM', 'Lest_X', 'Lest_Y', 'SoidukLiik', 'SoidukMark', 'SoidukVlAasta'], axis=1)
tartuliik.head()


# In[45]:


#liik = ['LIIKLUS']  
#tartuliik['SyndmusLiik'] = tartuliik.assign(SyndmusLiik='LIIKLUS')
tartuliik.insert(4, 'SyndmusLiik', 'LIIKLUS')
tartuliik.head()


# In[46]:


tartuliik.Kuupäev = pd.to_datetime(tartuliik.Kuupäev)
tartu_liik = tartuliik.groupby(['Kuupäev', 'SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']})
tartu_liik.head()


# In[47]:


#crime4[(crime4['ValdLinnNimetus'] == 'Tartu linn') & (crime4['ToimKpv'] == '2019-07-10')]
kuritegevus2 = tartu3.join(ilm1) # 2019
kuritegevus2.head()
kuritegevus2.tail()


# In[48]:


tartua19valikvaraliik = pd.concat([tartulinn, tartu2, tartuliik]) # 2019
tartua19valikvaraliik.Kuupäev = pd.to_datetime(tartua19valikvaraliik.Kuupäev)
tartu19 = tartua19valikvaraliik.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']})
tartu19.head(25)


# In[49]:


# 2019
sum = tartu19.groupby(['Kuupäev']).agg({('SyyteoLiik', 'count'): ['sum']})
mean = tartu19.groupby(['Kuupäev']).agg({('SyyteoLiik', 'count'): [np.mean]})
resultsum = tartu19.join(sum)
resultsum.head(25) # tartu everything crime 2019
tartu19 = resultsum.join(mean)
tartu19.head(250)


# In[50]:


tartu19 = tartu19.join(ilm1)
tartu19.head()


# In[51]:


# 2019
tartu19.reset_index(inplace=True)
tartu19.columns = ['Kuupäev', 'SyndmusLiik', 'SyyteoLiik', 'SyndmusLiik count', 'SyyteoLiik count',
       'SyyteoLiik sum', 'SyyteoLiik mean', 'Temperatuur max', 'Temperatuur min', 'Temperatuur mean',
       'Tuule kiirus max', 'Tuule kiirus min', 'Tuule kiirus mean', 'Sademed max', 'Sademed min',
       'Sademed mean', 'Sadanud lumi max', 'Sadanud lumi min', 'Sadanud lumi mean']

tartu19.describe()


# In[52]:


liiklus = pd.read_csv("liiklus_2.csv", sep="\t") # estonia traffic crime 2014-2018
liiklus = liiklus.rename(columns={"ToimKpv": "Kuupäev"})
tartuliikl = liiklus[(liiklus['ValdLinnNimetus'] == 'Tartu linn')]

#tartu = crime2[(crime2[13] == 'Tartu linn')]
tartuliikl = tartuliikl.drop(['Seadus', 'Paragrahv', 'ParagrahvTais',
       'Loige', 'Punkt', 'RikutudOigusnorm',
       'MaakondNimetus', 'KM', 'Lest_X', 'Lest_Y', 'SoidukLiik', 'SoidukMark', 'SoidukVlAasta'], axis=1)
tartuliikl.insert(4, 'SyndmusLiik', 'LIIKLUS')
tartuliikl = pd.concat([tartuliikl, tartuliik])
tartuliikl.Kuupäev = pd.to_datetime(tartuliikl.Kuupäev)
tartuliikl.head()
tartul = tartuliikl.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']})
tartul.head(25) # tartu everything crime 2014-2018


# In[53]:


sum = tartul.groupby(['Kuupäev']).agg({('SyyteoLiik', 'count'): ['sum']})
mean = tartul.groupby(['Kuupäev']).agg({('SyyteoLiik', 'count'): [np.mean]})
sum.head()
mean.head()
sum.columns
resultsum = tartul.join(sum)
resultsum.head(25) # tartu everything crime 2014-2018
tartul = resultsum.join(mean)
tartul.head(25)


# In[54]:


liiklus5 = tartul.join(ilm1)
liiklus5.head()


# In[55]:



liiklus5.reset_index(inplace=True)
liiklus5.columns = ['Kuupäev', 'SyndmusLiik', 'SyyteoLiik', 'SyndmusLiik count', 'SyyteoLiik count',
       'SyyteoLiik sum', 'SyyteoLiik mean', 'Temperatuur max', 'Temperatuur min', 'Temperatuur mean',
       'Tuule kiirus max', 'Tuule kiirus min', 'Tuule kiirus mean', 'Sademed max', 'Sademed min',
       'Sademed mean', 'Sadanud lumi max', 'Sadanud lumi min', 'Sadanud lumi mean']
liiklus5.head()


# In[56]:


tartuavalikvaraliik = pd.concat([tartuv, tartu, tartuliikl]) # tartu public, property and traffic crime 2014-2018
tartuavalikvaraliik.head(30)


# In[57]:


tartuavalikvaraliik.Kuupäev = pd.to_datetime(tartuavalikvaraliik.Kuupäev)
tartuavl = tartuavalikvaraliik.groupby(['Kuupäev','SyndmusLiik', 'SyyteoLiik']).agg({'SyndmusLiik': ['count'], 'SyyteoLiik': ['count']})
tartuavl.head(25) # tartu everything crime 2014-2018


# In[58]:


tartuavl.columns
tartuavl.columns.get_level_values(1)


# In[59]:


#tartuavl.xs('count', level=1, axis=1, drop_level=True)
#tartuavl.columns = tartuavl.columns.set_names([('arv', 'loend')])
#tartuavl.columns
#tartuavl.head(25)
#tartuavl.columns.get_level_values(0)


# In[60]:


sum = tartuavl.groupby(['Kuupäev']).agg({('SyyteoLiik', 'count'): ['sum']})
mean = tartuavl.groupby(['Kuupäev']).agg({('SyyteoLiik', 'count'): [np.mean]})
sum.head()
mean.head()
sum.columns


# In[61]:


resultsum = tartuavl.join(sum)
resultsum.head(25) # tartu everything crime 2014-2018
tartuavl = resultsum.join(mean)
tartuavl.head(25)


# In[62]:


result20 = tartuavl.join(ilm1) # tartu all crime with weather 2014-2018
result20.tail(30)


# In[63]:


#result20['Kuupäev'].describe()


# In[64]:


result20.describe()


# In[65]:


result20.columns


# In[66]:


#result20.index = result20.index.set_names(['Trial', 'measurement'])
result20.reset_index(inplace=True)
#result20[(  'SyndmusLiik', 'count')].sum
result20.head()


# In[67]:


result20.columns


# In[68]:


result20.columns.get_level_values(0)


# In[69]:


result20.rename({'Kuupäev':'Kuupäev', 'SyndmusLiik':'SyndmusLiik', 'SyyteoLiik':'SyyteoLiik', 'SyndmusLiik':'Syndmuste', 'SyyteoLiik':'Syytegude','SyyteoLiik':'Syytegusid', 'SyyteoLiik':'Syytegudest', 'Temperatuur':'Temperatuur', 'Temperatuur':'Temperatuuri', 'Temperatuur':'Temperatuurist','Tuule kiirus':'Tuule kiirus', 'Tuule kiirus':'Tuule kiiruse', 'Tuule kiirus':'Tuule kiirusest', 'Sademed':'Sademed', 'Sademed':'Sademete','Sademed':'Sademetest', 'Sadanud lumi':'Sadanud lumi', 'Sadanud lumi':'Sadanud lume', 'Sadanud lumi':'Sadanud lumest'}, level=0, axis='columns')
result20.head()


# In[70]:


result20.columns.get_level_values(0)


# In[71]:


#result20.rename({'':'Kuupäev', '':'SyndmusLiik', '':'SyyteoLiik', 'count':'count', 'count':'arv', 'count':'kokku', 'count':'keskmine', 'max':'max', 'min':'min', 'mean':'mean',
       #'max':'mxx', 'min':'mic', 'mean':'kesk', 'max':'suu', 'min':'väi', 'mean':'kskm', 'max':'roh', 'min':'väh', 'mean':'kskl'}, level=1, axis='columns')
result20.columns.get_level_values(1)


# In[72]:


result20.columns = ['Kuupäev', 'SyndmusLiik', 'SyyteoLiik', 'SyndmusLiik count', 'SyyteoLiik count',
       'SyyteoLiik sum', 'SyyteoLiik mean', 'Temperatuur max', 'Temperatuur min', 'Temperatuur mean',
       'Tuule kiirus max', 'Tuule kiirus min', 'Tuule kiirus mean', 'Sademed max', 'Sademed min',
       'Sademed mean', 'Sadanud lumi max', 'Sadanud lumi min', 'Sadanud lumi mean']
result20.head()


# In[73]:


result20['SyyteoLiik count'].sum()/5 # crimes in a year approximate


# In[74]:


(result20['SyyteoLiik count'].sum()/5)/365 # crimes in a day approximate


# In[75]:


result20['SyyteoLiik sum'].mean()


# In[76]:


result20['SyyteoLiik mean'].mean()


# In[77]:


result20['SyyteoLiik mean'].describe()


# In[78]:


max = result20[(result20['SyyteoLiik mean'] == 59)]
max


# In[79]:


result20['SyyteoLiik sum'].value_counts()


# In[80]:


result20['SyyteoLiik sum'].max()


# In[81]:


max = result20[(result20['SyyteoLiik sum'] == 96)]
max


# In[ ]:





# In[82]:


result20['Temperatuur min'].min()


# In[83]:


result20.describe()


# In[84]:


min = result20[(result20['Temperatuur min'] == -131.840910289)]
min


# In[85]:


result20.replace(-131.84091, -3.184091, inplace=True)
result20.describe()


# In[86]:


allyears = result20.append(tartu19)


# In[87]:


maxmean = allyears[(allyears['SyyteoLiik mean'] == 59.0)]
allyears['SyyteoLiik mean'].max()
maxmean


# In[88]:


type_series = result20['SyndmusLiik'].value_counts()/len(result20)
type_series.plot.bar(figsize=(30,5), rot=0, title='Crime 2014-2019')


# In[89]:


crosstab_df = pd.pivot_table(result20, values=
['SyyteoLiik mean'],index='Temperatuur mean',columns='Sademed mean')
crosstab_df


# In[90]:


#if crosstab_df is not ...:
   # for index, row in crosstab_df.iterrows():
     #   row.plot(figsize=(30,5), title=index)
      #  plt.show()


# In[91]:


a = plt.figure()
tyyp_series = result20['SyyteoLiik'].value_counts()/len(result20)
tyyp_series.plot.bar(rot=0, title='Crime 2014-2018')
a.savefig("crime 2014-18 VT KT.png", bbox_inches='tight', dpi=300)


# In[92]:


b = plt.figure()
tyyp_series1 = allyears['SyyteoLiik'].value_counts()/len(allyears)
tyyp_series1.plot.bar(rot=0, title='Crime 2014-2019')
b.savefig("crime 2014-19 VT KT.png", bbox_inches='tight', dpi=300)


# In[93]:


c = plt.figure()
tyyp_series2 = tartu19['SyyteoLiik'].value_counts()/len(tartu19)
tyyp_series2.plot.bar(rot=0, title='Crime 2019')
c.savefig("crime 2019 VT KT.png", bbox_inches='tight', dpi=300)


# In[94]:


d = plt.figure()
tyyp_series3 = ilmaliikluseta['SyyteoLiik'].value_counts()/len(ilmaliikluseta)
tyyp_series3.plot.bar(rot=0, title='Public and property crime 2014-2019')
d.savefig("public property 2014-19 VT KT.png", bbox_inches='tight', dpi=300)


# In[95]:



ilmacorr = ilmaliikluseta.corr()
ilmacorr


# In[96]:



allcorr = allyears.corr()
allcorr


# In[97]:


allyears.describe()


# In[98]:


corr = result20.corr()
corr


# In[99]:


corrliiklus = liiklus5.corr()
corrliiklus


# In[100]:


corr19 = tartu19.corr()
corr19


# In[101]:


import seaborn as sns
e = plt.figure()
sns.heatmap(corr, 
        xticklabels=corr.columns,
        yticklabels=corr.columns)
e.savefig("corr 2014-18.png", bbox_inches='tight', dpi=300)


# In[102]:


g = plt.figure()
sns.heatmap(ilmacorr, 
        xticklabels=ilmacorr.columns,
        yticklabels=ilmacorr.columns)
g.savefig("corr ilmaliikluseta.png", bbox_inches='tight', dpi=300)


# In[103]:


# kõik 2014-2019
h = plt.figure()
sns.heatmap(allcorr, 
        xticklabels=allcorr.columns,
        yticklabels=allcorr.columns)
h.savefig("corr 2014-19.png", bbox_inches='tight', dpi=300)


# In[104]:


# liiklus 2014-2018
i = plt.figure()
sns.heatmap(corrliiklus, 
        xticklabels=corrliiklus.columns,
        yticklabels=corrliiklus.columns)
i.savefig("corr ainult liiklus.png", bbox_inches='tight', dpi=300)


# In[105]:


# 2019
f = plt.figure()
sns.heatmap(corr19, 
        xticklabels=corr19.columns,
        yticklabels=corr19.columns)
f.savefig("corr 2019.png", bbox_inches='tight', dpi=300)


# In[106]:


kendall = result20.corr(method='kendall')
kendall


# In[107]:


sns.heatmap(kendall, 
        xticklabels=kendall.columns,
        yticklabels=kendall.columns)


# In[108]:


spearman = result20.corr(method='spearman')
spearman


# In[109]:


sns.heatmap(kendall, 
        xticklabels=kendall.columns,
        yticklabels=kendall.columns)


# In[110]:


#plt.scatter(result20['Temperatuur mean'], result20['SyyteoLiik mean']c='blue')
import seaborn as sns

sns.set(style="white")

jg = sns.relplot(x="Temperatuur mean", y="SyyteoLiik mean", hue="Temperatuur mean", size="SyyteoLiik mean",
            sizes=(40, 400), alpha=.5, palette="coolwarm",
            height=6, data=allyears)
jg.savefig("temp syytegu 2014-19.png", bbox_inches='tight', dpi=300)


# In[111]:


#plt.scatter(tartu19['Temperatuur mean'], tartu19['SyyteoLiik mean']c='blue')
sns.set(style="white")

fg = sns.relplot(x="Temperatuur mean", y="SyyteoLiik mean", hue="Temperatuur mean", size="SyyteoLiik mean",
            sizes=(40, 400), alpha=.5, palette="coolwarm",
            height=6, data=tartu19)
fg.savefig("teperatuur tartu19.png", bbox_inches='tight', dpi=300)


# In[112]:


#plt.scatter(result20['Sademed mean'], result20['SyyteoLiik mean'], c='blue')
sns.set(style="white")

df = sns.relplot(x="Sademed mean", y="SyyteoLiik mean", hue="Sademed mean", size="SyyteoLiik mean",
            sizes=(40, 400), alpha=.5, palette="winter_r",
            height=6, data=allyears)
df.savefig("sademed.png", bbox_inches='tight', dpi=300)


# In[113]:


#plt.scatter(tartu19['Sademed mean'], tartu19['SyyteoLiik mean'], c='blue')
sns.set(style="white")

cd = sns.relplot(x="Sademed mean", y="SyyteoLiik mean", hue="Sademed mean", size="SyyteoLiik mean",
            sizes=(40, 400), alpha=.5, palette="winter_r",
            height=6, data=tartu19)
cd.savefig("sadetartu19.png", bbox_inches='tight', dpi=300)


# In[114]:


#plt.scatter(result20['Tuule kiirus mean'], result20['SyyteoLiik mean'], c='blue')
sns.set(style="white")

bc = sns.relplot(x="Tuule kiirus mean", y="SyyteoLiik mean", hue="Tuule kiirus mean", size="SyyteoLiik mean",
            sizes=(40, 400), alpha=.5, palette="Greys",
            height=6, data=allyears)
bc.savefig("tuul.png", bbox_inches='tight', dpi=300)


# In[115]:


#plt.scatter(result20['Sadanud lumi mean'], result20['SyyteoLiik mean'], c='blue')
sns.set(style="white")

ab = sns.relplot(x="Sadanud lumi mean", y="SyyteoLiik mean", hue="Sadanud lumi mean", size="SyyteoLiik mean",
            sizes=(40, 400), alpha=.5, palette="Blues_r",
            height=6, data=allyears)
ab.savefig("lumi.png", bbox_inches='tight', dpi=300)


# In[116]:


#.plot(figsize=(18,5))
#na = result20.dropna()
#na.plot(figsize=(18,5))

result20['Kuupäev'].describe()
#plt.plot(result20.Kuupäev.to_pydatetime())
result20.plot(figsize=(30,5), x="Kuupäev", y=["SyyteoLiik mean", "Temperatuur mean"], title='2014-2019 crime mean')
k = plt.gcf()
k.savefig("2014-18.png", bbox_inches='tight', dpi=300)


# In[117]:



tartu19.plot(figsize=(30,5), x="Kuupäev", y=["SyyteoLiik mean", "Temperatuur mean"], title='2019 crime mean')
l = plt.gcf()
l.savefig("2019year.png", bbox_inches='tight', dpi=300)


# In[118]:



allyears.plot(figsize=(30,5), x="Kuupäev", y=["SyyteoLiik mean", "Temperatuur mean"], title='All crime mean')
m = plt.gcf()
m.savefig("2014-2019.png", bbox_inches='tight', dpi=300)


# In[119]:



allyears.plot(figsize=(30,5), x="Kuupäev", y=["SyyteoLiik sum", "Temperatuur mean"], title='All crime sum')
n = plt.gcf()
n.savefig("2014.19 crime sum.png", bbox_inches='tight', dpi=300)


# In[120]:


ilmaliikluseta.plot(figsize=(30,5), x="Kuupäev", y=["SyyteoLiik mean", "Temperatuur mean"], title='Public and property crime')
np = plt.gcf()

np.savefig("2014.19 liikluseta crime mean.png", bbox_inches='tight', dpi=300)


# In[121]:


liiklus5.plot(figsize=(30,5), x="Kuupäev", y=["SyyteoLiik mean", "Temperatuur mean"], title='Traffic crime')
sp = plt.gcf()
sp.savefig("2014.19 liiklus.png", bbox_inches='tight', dpi=300)


# In[122]:


from sklearn.model_selection import train_test_split
result20 = result20.dropna()
syy = result20['SyyteoLiik mean']
temp = result20['Temperatuur mean']
X_train, X_test, y_train, y_test = train_test_split(syy.values, temp.values, train_size=0.7, test_size=0.3, random_state=0)


# In[123]:


from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
from ipykernel import kernelapp as app
lab_enc = preprocessing.LabelEncoder()
y_train = lab_enc.fit_transform(y_train)
X_train = lab_enc.fit_transform(X_train)
y_test = lab_enc.fit_transform(y_test)
X_test = lab_enc.fit_transform(X_test)
y_train = y_train.reshape(-1, 1)
X_train = X_train.reshape(-1, 1)
y_test = y_test.reshape(-1, 1)
X_test = X_test.reshape(-1, 1)
results_df = pd.DataFrame(columns=['model', 'accuracy'])
model = KNeighborsClassifier(n_neighbors = 1)
model.fit(X_train, y_train.ravel())
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '1-NN', 'accuracy': acc }, ignore_index=True)
results_df


# In[124]:


model = KNeighborsClassifier(n_neighbors = 3)
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '3-NN', 'accuracy': acc }, ignore_index=True)
model = KNeighborsClassifier(n_neighbors = 5)
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '5-NN', 'accuracy': acc }, ignore_index=True)
model = KNeighborsClassifier(n_neighbors = 11)
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '11-NN', 'accuracy': acc }, ignore_index=True)
results_df


# In[125]:


model = KNeighborsClassifier(n_neighbors = 1, metric='manhattan')
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '1-NNm', 'accuracy': acc }, ignore_index=True)
model = KNeighborsClassifier(n_neighbors = 3, metric='manhattan')
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '3-NNm', 'accuracy': acc }, ignore_index=True)
model = KNeighborsClassifier(n_neighbors = 5, metric='manhattan')
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '5-NNm', 'accuracy': acc }, ignore_index=True)
model = KNeighborsClassifier(n_neighbors = 11, metric='manhattan')
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '11-NNm', 'accuracy': acc }, ignore_index=True)
results_df


# In[126]:


from sklearn.tree import DecisionTreeClassifier

model = DecisionTreeClassifier(random_state=0, criterion='gini')
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': 'DT', 'accuracy': acc }, ignore_index=True)
model = DecisionTreeClassifier(random_state=3, criterion='gini')
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '3-DT', 'accuracy': acc }, ignore_index=True)
model = DecisionTreeClassifier(random_state=0, criterion='entropy')
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': 'DTe', 'accuracy': acc }, ignore_index=True)
model = DecisionTreeClassifier(random_state=0, min_samples_leaf = 10, criterion='entropy')
model.fit(X_train, y_train)
acc = accuracy_score(y_test, model.predict(X_test))
results_df = results_df.append({'model': '10-DT', 'accuracy': acc }, ignore_index=True)
results_df


# In[127]:


mask = (tartu19['Kuupäev'] > '2019-07-14') & (tartu19['Kuupäev'] <= '2019-07-20')
metallica = tartu19.loc[mask]
metallica


# In[128]:


metallica.describe()


# In[129]:


metallica.plot(figsize=(30,5), x="Kuupäev", y=["SyyteoLiik mean", "Temperatuur mean"], title='Metallica')
met = plt.gcf()
met.savefig("metallica.png", bbox_inches='tight', dpi=300)


# In[130]:


metallica.plot(figsize=(30,5), x="Kuupäev", y=["SyyteoLiik sum", "Temperatuur mean"], title='Metallica')
meta = plt.gcf()
meta.savefig("metallica crime sum.png", bbox_inches='tight', dpi=300)


# In[131]:


mask = (tartulinn['Kuupäev'] > '2019-07-14') & (tartulinn['Kuupäev'] <= '2019-07-20')
metallica2 = tartulinn.loc[mask]
metallica2.head(25)


# In[132]:


mask = (tartu2['Kuupäev'] > '2019-07-14') & (tartu2['Kuupäev'] <= '2019-07-20')
metallica2 = tartu2.loc[mask]
metallica2.head(25)


# In[133]:


mask = (tartuliik['Kuupäev'] > '2019-07-14') & (tartuliik['Kuupäev'] <= '2019-07-20')
metallica2 = tartuliik.loc[mask]
metallica2.head(205)


# In[134]:


mask = (ilmaliikluseta['Kuupäev'] > '2019-07-14') & (ilmaliikluseta['Kuupäev'] <= '2019-07-20')
metallica3 = ilmaliikluseta.loc[mask]
metallica3.head(25)


# In[135]:


metallica3.describe()


# In[136]:


KT = metallica3[(metallica3['SyyteoLiik'] == 'KT')]
KT


# In[137]:


VT = metallica3[(metallica3['SyyteoLiik'] != 'KT')]
VT


# In[138]:


o = plt.figure()
tyyp_series4 = metallica3['SyyteoLiik'].value_counts()/len(metallica3)
tyyp_series4.plot.bar(rot=0,title='Public and property crime during Metallica')
o.savefig("public property metallica.png", bbox_inches='tight', dpi=300)


# In[139]:


p = plt.figure()
tyyp_series5 = metallica['SyyteoLiik'].value_counts()/len(metallica)
tyyp_series5.plot.bar(rot=0, title='All crime during Metallica')

p.savefig("all crime metallica.png", bbox_inches='tight', dpi=300)


# In[140]:


r = plt.figure()
tyyp_series6 = metallica2['SyyteoLiik'].value_counts()/len(metallica2)
tyyp_series6.plot.bar(rot=0, title='Traffic crime during Metallica')
r.savefig("traffic metallica.png", bbox_inches='tight', dpi=300)


# In[141]:


metallica.describe()


# In[142]:


metallica['SyndmusLiik count'].mean()


# In[143]:


metallica['SyyteoLiik count'].sum()


# In[144]:


metallica.head(20)


# In[145]:



metal = metallica['SyyteoLiik mean'].mean()
metalkokku = metallica['SyyteoLiik sum'].mean()
muidu = allyears['SyyteoLiik mean'].mean()
muidukokku = allyears['SyyteoLiik sum'].mean()
aasta = tartu19['SyyteoLiik mean'].mean()
aastakokku = tartu19['SyyteoLiik sum'].mean()
eelmised = result20['SyyteoLiik mean'].mean()
eelmisedkokku = result20['SyyteoLiik sum'].mean()
keskmine = [metal, muidu, aasta, eelmised]
summa = [metalkokku, muidukokku, aastakokku, eelmisedkokku]
index = ['Metallica', '2014-2019', '2019',
          '2014-2018']
uus = pd.DataFrame({'Crime mean in a day': keskmine,
                    'Crime sum in a day': summa}, index=index)
ax = uus.plot.bar(rot=0, title='All crime in Tartu')
kr = plt.gcf()
kr.savefig("meansn metallica.png", bbox_inches='tight', dpi=300)


# In[146]:


mask = (liiklus5['Kuupäev'] > '2019-07-14') & (liiklus5['Kuupäev'] <= '2019-07-20')
metallica2 = liiklus5.loc[mask]
metallica2.head(20)


# In[147]:


tartu_liik.tail()


# In[148]:



metal1 = metallica2['SyyteoLiik mean'].mean()
metalkokku1 = metallica2['SyyteoLiik sum'].mean()
muidu1 = liiklus5['SyyteoLiik mean'].mean()
muidukokku1 = liiklus5['SyyteoLiik sum'].mean()
metal2 = metallica3['SyyteoLiik mean'].mean()
metalkokku2 = metallica3['SyyteoLiik sum'].mean()
ilmaliik = ilmaliikluseta['SyyteoLiik mean'].mean()
ilmaliikkokku = ilmaliikluseta['SyyteoLiik sum'].mean()
keskmine = [metal1, muidu1, metal2, ilmaliik]
summa = [metalkokku1, muidukokku1, metalkokku2, ilmaliikkokku]
index = ['Metallica traffic', 'Traffic', 'Metallica other',
          'Other crime']
uus1 = pd.DataFrame({'Crime mean in a day': keskmine,
                    'Crime sum in a day': summa}, index=index)
ax1 = uus1.plot.bar(rot=0, title='Traffic crime and public and property crime separated')
mt = plt.gcf()
mt.savefig("traffic and not metallica.png", bbox_inches='tight', dpi=300)


# In[149]:


allyears.describe()


# In[ ]:





# In[ ]:




